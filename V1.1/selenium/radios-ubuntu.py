# pas d'interface graphique -> automatisation chercher l'information depuis le serveru fournisseur
# améliore les performances, on a un bot et pas de client, problème en temps réel car beaucoup de requetes(5000 requetes par secondes)
# exécution avec python3 radios-linuxpedia.py
from selenium import webdriver
# pour le fichier json
import json
driver = webdriver.Firefox() 

driver.get("https://doc.ubuntu-fr.org/liste_radio_france")
elements = driver.find_elements_by_css_selector("li.node")
result = []

for element in elements:
    title = element.find_element_by_css_selector("div.li > a.urlextern")
    title = title.text
    try:
        url = element.find_element_by_css_selector(
            "ul.fix-media-list-overlap > li.level2 > div.li > a.urlextern")
        url = url.get_attribute("href")
    except:
        url = ""
    item = {"nom-radio": title, "flux": url}
    result.append(item)

with open("radios-ubuntu.json", "w") as f:
    json.dump(result, f, indent=2, sort_keys=True)
# on ferme le driver
driver.close()
