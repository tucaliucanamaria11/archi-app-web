import vlc
import time
# install pip install python-vlc
# https://www.programcreek.com/python/example/93375/vlc.Instance

playlist = ['http://streaming.radio.rtl2.fr/rtl2-1-44-128', '/path/to/song2.flac', 'path/to/song3.flac']

for song in playlist:
    player = vlc.MediaPlayer(song)
    player.play()
    time.sleep(10)
