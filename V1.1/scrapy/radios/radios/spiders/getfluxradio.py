# -*- coding: utf-8 -*-
import scrapy


class ToScrapeCSSSpider(scrapy.Spider):
    name = "getfluxradio"
    start_urls = [
        'http://127.0.0.1/radio.html',
    ]

    def parse(self, response):
        for url in response.css("li.node"):
            yield {
                "nom-radio": url.css("div.li > a.urlextern::text").extract_first(),
                "flux": url.css("ul.fix-media-list-overlap > li.level2 > div.li > a.urlextern::attr(href)").extract_first()
            }

